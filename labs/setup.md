# Lab Setup
Prior to running this lab, there are a few operations that need to be done on the environment.

This lab was built against an Openshift 3.11 environment with PVs available.  

## Installing CodeReady Workspaces
This step installs CodeReady Workspaces into your OpenShift cluster.

1. Download CodeReady Workspaces
The current version of CodeReady can be found [here](https://developers.redhat.com/products/codeready-workspaces/getting-started/)
Download this archive to your local computer.  We recommend putting this in a separate directory.

1. Unpack the archive
Unzip the downloaded file. You will see a .yaml file, a couple of shell scripts, and a README file.

1. Log into OpenShift as the Cluster Admin
From the same directory you unzipped the installer files in, log into OpenShift as the cluster Admin using the `oc login` command

1. Install CodeReady
Once logged in, simply run the deploy script.  For Linux systems, it would be `./deploy.sh --deploy`

After about five minutes, CodeReady will be installed.  The URL to the login page will be displayed at the end of the script.

For reference, the namespace created is `workspaces`

## Installing a Custom Jenkins Image
The default Jenkins image that comes with OpenShift works just fine, but deploys rather slow.  Because we're all impatient (who isn't?), we modified the default template and gave Jenkins 1 CPU, which cut the deployment time from 10 minutes to about 4 minutes.  For the curious, this was the section of the default Jenkins template that was modified:

    "resources": {
      "limits": {
        "cpu": "1",
        "memory": "${MEMORY_LIMIT}"
      }
    },

To import this new template and make available to all users, do the following steps:

1. Download the updated [template](https://gitlab.com/redhatsummitlabs/transitioning-existing-applications-to-containers-its-easier-than-you-think/raw/master/templates/jenkins-ephemeral-summit.json)

1. Log into OpenShift as the Cluster Admin using the `oc login` command

1. Import the Template
From the same directory in which you downloaded the template, run the following commands

    `oc create -f jenkins-ephemeral-summit.json -n openshift`

This will create a new Jenkins image in the openshift namespace, meaning it will be available to all users.

## A Note About GUIDS
Throughout the labs, you will see mention of GUIDS.  The URL convention used is specific to the Summit Labs and how CloudForms provisions repeatable infrastructure from predefined templates.  The URLs for your environment will be different.
