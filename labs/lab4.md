# Lab IV - Adding Jenkins

This lab will quickly show how Jenkins can be integrated into your project. This lab will be done from within the Web Console of OpenShift.  This will be a brief introduction with the intent in getting a minimal Jenkins install working.

## Table of Contents
1. [Create a Jenkins Instance](#create-a-jenkins-instance)
1. [Configure Jenkins](#configure-jenkins)
1. [Create a Build Pipeline](#create-a-build-pipeline)
1. [Cleanup](#cleanup)

## Create a Jenkins Instance
Now we will add a Jenkins instance to the project.

1. We're going to add Jenkins to the Tomcat project we built in [Lab 2](/labs/lab2.md).  Navigate into the project by clicking the project name on the OpenShift main page
![](/images/lab-04-select-project.png)

1. On the upper navigation bar of your project is a drop down titled "Add to Project".  
![](/images/lab04-add-jenkins.png)
Select the "Browse Catalog Option"

1. Scroll to Select the CI/CD tab, then click `Jenkins (Ephemeral) - Red Hat Summit`
![](/images/lab-04-select-jenkins.png)

1. This will bring up the wizard to create a Jenkins Master in your environment.
  1. On the first screen (Information), simply hit the `Next` button
  ![](/images/lab-04-jenkins-information.png)

  1. On the second screen (Configuration), again, leave all values blank and hit the `Next` button.
  ![](/images/lab-04-jenkins-configuration.png)

     > This is where general configuation would go.  One field to note is the `Enable OAuth in Jenkins`.  This will automatically tie Jenkins authorization back to OpenShift for the login process.

  1. On the third screen (Binding), simply hit the `Create` button.  This will start the provisioning process.  Click the `Continue to the project overview` link to see the new state of the project
  ![](/images/lab-04-jenkins-provisioning.png)

## Configure Jenkins
1. Provisioning the Jenkins master can take up to five minutes as a lot of infrastructure will be deployed and spun up.  When the number of Pods goes to 1, you can follow the link to the Jenkins login screen.  Note - it still may take a minute or so for the actual Jenkins application to complete startup and accept traffic - be patient.
![](/images/lab-04-jenkins-provisioned.png)

1. You will see the Jenkins login screen:
![](/images/lab-04-jenkins-login.png)

1. Click the **Log in with OpenShift** button

1. This will show the OpenShift login screen.  Use your lab credentials assigned (userxx)

1. On the next screen, click the `Allow selected permissions` button
![](/images/lab-04-openshift-auth.png)

   > It may take some time before you actually get to the Jenkins dashboard as underlying infrastructure is provisioned for the first time.

## Create a Build Pipeline
1. On the Jenkins Home page, click "New Item"
![](/images/lab-04-jenkins-home-screen.png)

1. Enter a name (i.e. `jenkins-tomcat-webapp`), click "Freestyle Project", and select "OK" at the bottom of the screen.
![](/images/lab-04-jenkins-new-item.png)

1. We now have an empty Jenkins project with the OpenShift pipeline plugins available to configure how the build will be managed.
    * Clear the `Restrict where this project can be run` checkbox
    * Under Source Code Management, select Git and enter the URL to the repository we have been using `https://github.com/egetchel/summit-lab1.git`
    * Next we will define the steps that Jenkins will take to build the application.  We will be working against the actual Tomcat web application within in our Project.  In the 'Build' section of the Jenkins configuration screen, enter the following:
        * Click the "Add build step" and select Trigger OpenShift Build - enter "jws-app" as the name of the Build Config to trigger.
        * Click the "Add build step" and select Trigger OpenShift Deployment - enter "jws-app" as the Deployment Config
        * Click the "Add build step" and select Scale OpenShift Deployment - enter "jws-app" as the Deployment Config and "1" as the number of replicas  

We have just defined a Jenkins managed job to interact with OpenShift to build, containerize, and deploy the web application.  Click the "Save" button.

On the left-hand navigation panel, Click "Build Now"

Navigate back to your OpenShift console, and on the Application Overview, you will see a new version of the Tomcat web application being built and deployed.

This is obviously a minimalistic example of integrating with Jenkins.  Other steps to the build process can easily be added, such as automatically building when code is committed, executing tests, promoting images between environments, or handing the process off to another Jenkins job.

## Cleanup
You can go ahead and delete the Tomcat Web App project as we will be moving on to other technologies in the [next lab](/labs/lab5.md).
